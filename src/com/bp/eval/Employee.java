/**
 * 
 */
package com.bp.eval;

import java.util.Date;

/**
 * @author SHAON
 *  Class to hold the employee details
 */
public class Employee {
	
	/*
	 * Variable to hold the employee id
	 */
	private int id;
	/*
	 * Variable to hold the employee name
	 */
	private String name;
	/*
	 * Variable to hold the employee's hired date
	 */
	private Date dateHired;
	/*
	 * Variable to hold if the employee is manager or not
	 */
	private boolean isManagerFlag;
	
	/*
	 * New Variable to hold if the employee is partime or fulltime
	 */
	private boolean partTimeEmployeeFlg;
	
	/*
	 * Getter methods for the above variables
	 */
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public Date getDateHired() {
		return dateHired;
	}
	
	public boolean isManagerFlag() {
		return isManagerFlag;
	}
	
	public boolean isPartTimeEmployeeFlg() {
		return partTimeEmployeeFlg;
	}
	
	

}
