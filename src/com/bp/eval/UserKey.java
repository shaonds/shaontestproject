/**
 * 
 */
package com.bp.eval;

import java.io.Serializable;

/**
 * @author SHAON
 *
 */


/*
 * Class to act as a Key in a Hashtable of a Servlet session 
 */
public class UserKey implements Serializable{
	
	private static final long serialVersionUID = 3712542458983553929L;
	
	//Variable to hold the name of the user
	private String name;
	// Variable to hold the userid of the user
	private String userid;

	/*
	 * 2 argument Constructor to initialize the UserKey instance
	 */
	public UserKey(String name, String userid) {
		this.name = name;
		this.userid = userid;
	}

	/*
	 * getter method for the varaible 'name'
	 */
	public String getName() {
		return name;
	}

	/*
	 * getter method for the varaible 'userid'
	 */
	public String getUserID() {
		return userid;
	}

	/*
	 * Overridden method to compare the UserKey object instances
	 * to check if they are equal or not
	 */
	@Override
	public boolean equals(Object o){
	    
		if(o == null){
	    	return false;
	    }
	    if(!(o instanceof UserKey)){
	    	return false;
	    }

	    UserKey other = (UserKey) o;
	    if(this.userid != other.userid) {
	    	return false;
	    }
	    if(! this.name.equals(other.name)){
	    	return false;
	    }

	    return true;
	  }
	
	/*
	 * Overridden method to calculate the hashcode
	 *  of UserKey object instance
	 */
	@Override
	public int hashCode(){
	   
		int result = 13;
		result = 7 * result + this.name.hashCode();
		result = 7 * result + this.userid.hashCode();
		
		return result;
	  }
	
	/**
	 * Main method to test the expected functionality
	 * @param args
	 */
	public static void main(String[] args) {

		UserKey b1 = new UserKey("Bill Smith", "BSMITH");
		UserKey b2 = new UserKey("Bill Smith", "BSMITH");
		UserKey b3 = new UserKey("Susan Smith", "SSMITH");
		UserKey b4 = new UserKey(null,null);

		System.out.println("b1.equals(b1) :" + b1.equals(b1) );  // prints true
		System.out.println("b1.equals(b2) :" + b1.equals(b2) );  // prints true
		System.out.println("b1.equals(b3) :" + b1.equals(b3) );  // prints false
		System.out.println("b1.equals(null) :"+  b1.equals(null) ); // prints false
		System.out.println("b1.equals(\"Some String\":) :" + b1.equals("Some String:") ); // prints false
		System.out.println("b4.equals(b1) :" + b4.equals(b1) ); // prints false

		java.util.Hashtable ht = new java.util.Hashtable();
		ht.put(b1,"Some Data");

		String s = (String) ht.get(b2);

		System.out.println("s.equals(\"Some Data\") :" + s.equals("Some Data") );  // prints true

	}

}
