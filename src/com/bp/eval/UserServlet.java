package com.bp.eval;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {


	private static final long serialVersionUID = -5595304705989031654L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		System.out.println("doGet .....");
		
		HttpSession session = req.getSession();
		
		System.out.println("session ==> "+session.getId());
		
		
		UserKey b1 = new UserKey("Bill Smith", "BSMITH");
		UserKey b2 = new UserKey("Bill Smith", "BSMITH");
		UserKey b3 = new UserKey("Susan Smith", "SSMITH");
		UserKey b4 = new UserKey(null,null);

		session.setAttribute("b1", b1);
		session.setAttribute("b2", b2);
		session.setAttribute("b3", b3);
		session.setAttribute("b4", b4);
		
		System.out.println("b1.equals(b1) :" + b1.equals(b1) );  // prints true
		System.out.println("b1.equals(b2) :" + b1.equals(b2) );  // prints true
		System.out.println("b1.equals(b3) :" + b1.equals(b3) );  // prints false
		System.out.println("b1.equals(null) :"+  b1.equals(null) ); // prints false
		System.out.println("b1.equals(\"Some String\":) :" + b1.equals("Some String:") ); // prints false
		System.out.println("b4.equals(b1) :" + b4.equals(b1) ); // prints false
		
		Hashtable<UserKey, String> ht = new Hashtable<UserKey, String>();
		ht.put(b1,"Some Data");
		
		String s = (String) ht.get(session.getAttribute("b1"));
		
		PrintWriter writer = resp.getWriter();
		
		writer.println("Hello...."+s);
	}
}
