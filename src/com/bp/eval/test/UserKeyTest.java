package com.bp.eval.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.bp.eval.UserKey;

public class UserKeyTest {
	
	/*
	 * Method to test getName method
	 */
	@Test
	public void testGetName(){		
		UserKey usr1 = new UserKey("Bill Smith", "BSMITH");
		assertEquals("Bill Smith", usr1.getName());
	}
	
	/*
	 * Method to test getUserID method
	 */
	@Test
	public void testGetUserID(){		
		UserKey usr1 = new UserKey("Bill Smith", "BSMITH");
		assertEquals("BSMITH", usr1.getUserID());
	}
	
	/*
	 * Method to test equals method
	 */
	@Test
	public void testEquals(){		
		UserKey b1 = new UserKey("Bill Smith", "BSMITH");
		UserKey b2 = new UserKey("Bill Smith", "BSMITH");
		assertTrue(b1.equals(b2));
		
		UserKey b3 = new UserKey("Bill Smith1", "BSMITH1");
		assertFalse(b1.equals(b3));
	}
	
	/*
	 * Method to test hashCode method
	 */
	@Test
	public void testHashCode(){		
		UserKey b1 = new UserKey("Bill Smith", "BSMITH");
		UserKey b2 = new UserKey("Bill Smith", "BSMITH");
		assertEquals(b1.hashCode(), b2.hashCode());
		
		UserKey b3 = new UserKey("Bill Smith1", "BSMITH1");
		assertFalse(b1.hashCode()==b3.hashCode());
	}
	
	/*
	 * Method to test if it is Serializable
	 */
	@Test
	public void testSerializable(){
		UserKey b1 = new UserKey("Bill Smith", "BSMITH");
		
		assertTrue(b1 instanceof java.io.Serializable || b1 instanceof java.io.Externalizable);
	}

}
